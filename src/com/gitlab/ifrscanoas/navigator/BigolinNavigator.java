/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.ifrscanoas.navigator;

import java.io.File;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author developer
 */
public class BigolinNavigator extends Application {

    private Scene scene;
    private int count = 1;
    private String str = "";
    private Logger l = new Logger();
    private int easterEgg = 0;
    Browser browser = new Browser();

    @Override
    public void start(Stage stage) {
        // create the scene
        stage.setTitle("Web View");
   
        scene = new Scene(browser, 750, 500, Color.web("#666970"));
        stage.setFullScreen(true);
        stage.setFullScreenExitHint("Você jamais sairá");
    
        stage.setScene(scene);
        monitoraTudo(stage);
        verifica(stage);
        stage.show();
    }
    
    private void verifica(Stage stage){
        stage.setOnHidden(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                stage.requestFocus();
            }
        });
    }

    private void monitoraTudo(Stage stage) {
        stage.setFullScreenExitKeyCombination(KeyCombination.NO_MA‌TCH);
        stage.getScene().setOnKeyPressed(new EventHandler<KeyEvent>() {

            public void handle(KeyEvent ke) {
                if (ke.getCode() == KeyCode.ALT) {
                    Alert alert;
                    if(easterEgg > 5){
                        alert = new Alert(AlertType.ERROR);   
                        
                        String musicFile = "siren.mp3";     // For example

                        Media sound = new Media(new File(musicFile).toURI().toString());
                        MediaPlayer mediaPlayer = new MediaPlayer(sound);
                        mediaPlayer.play();
                    }else{
                        alert = new Alert(AlertType.INFORMATION);
                    }                  
                   
                    alert.setTitle("I SEE");
                    alert.setHeaderText("Você não deveria fazer isso");
                    alert.setContentText("Esse é o aviso nº " + easterEgg);
                    
                    alert.showAndWait();
                    stage.requestFocus();
                    //return ;
                    easterEgg++;

                }
                count++;
                str += ke.getCode();
                if(count > 10 && browser.getUrl() != "https://moodle.canoas.ifrs.edu.br/login/index.php"){
                    System.out.println("----");
                    System.out.println(browser.getUrl());
                    System.out.println(str);
                    l.escreve(str);
                    str = " ";
                    count = 1;
                }                
            }
        });
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
